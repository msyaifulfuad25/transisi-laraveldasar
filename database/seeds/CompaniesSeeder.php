<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name' => 'Apple',
            'email' => 'apple@gmail.com',
            'logo' => '1593884863.png',
            'website' => 'apple.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('companies')->insert([
            'name' => 'Yamaha',
            'email' => 'yamaha@gmail.com',
            'logo' => '1593884888.png',
            'website' => 'yamaha.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('companies')->insert([
            'name' => 'Orang Tua',
            'email' => 'ot@gmail.com',
            'logo' => '1593884926.png',
            'website' => 'ot.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('companies')->insert([
            'name' => 'Panasonic',
            'email' => 'panasonic@gmail.com',
            'logo' => '1593884952.png',
            'website' => 'panasonic.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('companies')->insert([
            'name' => 'United Tractor',
            'email' => 'ut@gmail.com',
            'logo' => '1593884993.png',
            'website' => 'ut.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('companies')->insert([
            'name' => 'Pertamina',
            'email' => 'pertamina@gmail.com',
            'logo' => '1593885021.png',
            'website' => 'pertamina.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('companies')->insert([
            'name' => 'Garuda Food',
            'email' => 'gf@gmail.com',
            'logo' => '1593889410.png',
            'website' => 'gf.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('companies')->insert([
            'name' => 'Gudang Garam',
            'email' => 'gg@gmail.com',
            'logo' => '1593891380.png',
            'website' => 'gg.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        // factory(App\Companies::class,100)->create();
    }
}
