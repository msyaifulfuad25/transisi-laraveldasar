<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'name' => 'Bimo Putra Dwitya',
            'companies_id' => '1',
            'email' => 'bpd@gmail.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('employees')->insert([
            'name' => 'Tony Q',
            'companies_id' => '4',
            'email' => 'tq@gmail.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('employees')->insert([
            'name' => 'Han Farhani',
            'companies_id' => '5',
            'email' => 'hf@gmail.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('employees')->insert([
            'name' => 'Febinda Tito',
            'companies_id' => '2',
            'email' => 'ft@gmail.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('employees')->insert([
            'name' => 'Happy Asmara',
            'companies_id' => '3',
            'email' => 'ha@gmail.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('employees')->insert([
            'name' => 'Tretan Muslim',
            'companies_id' => '4',
            'email' => 'tm@gmail.com',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
