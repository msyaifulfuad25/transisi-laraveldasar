<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Companies;
use Illuminate\Support\Facades\Storage;
use File;

class CompaniesController extends Controller
{

    public function __construct() {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = DB::table('companies')->paginate(5);
        return view('companies.index', ['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:companies',
            'logo' => 'required|dimensions:min_width=100,min_height=100|mimes:png|max:2048',
            'website' => 'required',
        ]);

        $file = $request->file('logo');
        $logo = time().'.'.request()->logo->getClientOriginalExtension();
     
        $companies = new Companies([
            'name' => $request->get('name'),
            'email'=> $request->get('email'),
            'logo'=> $logo,
            'website'=> $request->get('website')
        ]);
        $companies->save();
        // print_r($file->getRealPath());die;
        // request()->logo->move(public_path('images'), $logo);
        Storage::disk('local')->put('company'.'/'.$logo, File::get($file));
        return redirect('/companies')->with('success', 'Companies has been added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $companies = Companies::find($id);
        return view('companies.detail', ['companies' => $companies]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companies = Companies::find($id);
        return view('companies.edit', ['companies' => $companies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $companies = Companies::find($id);

        if (@$request->all()['logo'] != null) {
            if ($companies->email == $request->get('email')) {
                $request->validate([
                    'name' => 'required',
                    'email' => 'required|email',
                    'logo' => 'required|dimensions:min_width=100,min_height=100|mimes:png|max:2048',
                    'website' => 'required',
                ]);
            } else {
                $request->validate([
                    'name' => 'required',
                    'email' => 'required|email|unique:companies,email,{$this->post->id}',
                    'logo' => 'required|dimensions:min_width=100,min_height=100|mimes:png|max:2048',
                    'website' => 'required',
                ]);
            }
        } else {
            if ($companies->email == $request->get('email')) {
                $request->validate([
                    'name' => 'required',
                    'email' => 'required|email',
                    'website' => 'required',
                ]);
            } else {
                $request->validate([
                    'name' => 'required',
                    'email' => 'required|email|unique:companies,email,{$this->post->id}',
                    'website' => 'required',
                ]);
            }
        }

        $companies->name = $request->get('name');
        $companies->email = $request->get('email');
        $companies->website = $request->get('website');
        $old_logo = $companies->logo;

        if (@$request->all()['logo'] != null) {
            $file = $request->file('logo');
            $logo = time().'.'.request()->logo->getClientOriginalExtension();
            $companies->logo = $logo;
            Storage::disk('local')->put('company'.'/'.$logo, File::get($file));
            Storage::delete('/public/company/'.$old_logo); 
        }
        $companies->update();

        return redirect('/companies')->with('success', 'Companies has been updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $companies = Companies::find($id);
        Storage::delete('/public/company/'.$companies->logo); 
        $companies->delete();

        return redirect('/companies')->with('success', 'Companies has been deleted Successfully');
    }
}
