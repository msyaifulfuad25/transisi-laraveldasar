<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Employees;
use App\Companies;

class EmployeesController extends Controller
{
    
    public function __construct() {
        $this->middleware("auth");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = DB::table('employees')->select('employees.id', 'employees.name as employees_name', 'companies.id as companies_id', 'companies.name as companies_name', 'employees.email')->join('companies', 'companies.id', '=', 'employees.companies_id')->paginate(5);
        return view('employees.index', ['employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Companies::all('name', 'id');
        return view('employees.create', ['items' => $items]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'companies_id' => 'required',
            'email' => 'required|unique:employees',
        ]);
     
        $employees = new Employees([
            'name' => $request->get('name'),
            'companies_id'=> $request->get('companies_id'),
            'email'=> $request->get('email'),
        ]);
        $employees->save();
        return redirect('/employees')->with('success', 'Employees has been added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = Employees::find($id);
        $items = Companies::all('name', 'id');
        return view('employees.edit', ['employees' => $employees, 'items' => $items]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employees = Employees::find($id);

        if ($employees->email == $request->get('email')) {
            $request->validate([
                'name' => 'required',
                'companies_id' => 'required',
                'email' => 'required|email',
            ]);
        } else {
            $request->validate([
                'name' => 'required',
                'companies_id' => 'required',
                'email' => 'required|email|unique:employees,email,{$this->post->id}',
            ]);
        }
     
        $employees->name = $request->get('name');
        $employees->companies_id = $request->get('companies_id');
        $employees->email = $request->get('email');
        $employees->update();
        return redirect('/employees')->with('success', 'Employees has been updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employees = Employees::find($id);
        $employees->delete();

        return redirect('/employees')->with('success', 'Employees has been deleted Successfully');
    }
}
