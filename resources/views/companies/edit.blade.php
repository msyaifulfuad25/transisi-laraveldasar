@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        Companies
        <small>preview of simple tables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Companies</a></li>
        <li class="active">Companies Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Companies Edit</h3>
            </div>
            <!-- /.box-header -->

            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
              </div><br />
            @endif
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="{{ route('companies.update', $companies->id) }}" enctype="multipart/form-data">
              {{ method_field('PUT') }}
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $companies->name }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $companies->email }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="logo" class="col-sm-2 control-label">Logo</label>
                  <div class="col-sm-8">
                    <input type="file" id="logo" class="form-control" name="logo" placeholder="Logo" value="{{ $companies->logo }}">
                    <br>
                    <img src="{{ asset('storage/company/').'/'.$companies->logo }}" id="preview-logo" width="200px" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="website" class="col-sm-2 control-label">Website</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="website" placeholder="Website" value="{{ $companies->website }}">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{ route('companies.index') }}" type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('scripts')
<script type="text/javascript">
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          
          reader.onload = function (e) {
              $('#preview-logo').attr('src', e.target.result);
          }
          
          reader.readAsDataURL(input.files[0]);
      }
  }
  
  $("#logo").change(function(){
      readURL(this);
  });
</script>
@endsection