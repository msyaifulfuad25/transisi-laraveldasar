@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        Companies
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Companies</a></li>
        <li class="active">Companies Detail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Companies Detail</h3>
            </div>
            <!-- /.box-header -->

            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
              </div><br />
            @endif
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="{{ route('companies.store') }}" enctype="multipart/form-data">
              @csrf
              <div class="box-body">
                <div class="col-md-6">
                  <table class="table">
                    <tr>
                      <th>Name</th>
                      <td>{{ $companies->name }}</td>
                    </tr>
                    <tr>
                      <th>Email</th>
                      <td>{{ $companies->email }}</td>
                    </tr>
                    <tr>
                      <th>Logo</th>
                      <td>
                        {{ $companies->logo }}
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><img src="{{ asset('storage/company/').'/'.$companies->logo }}" id="preview-logo" width="200px" /></td>
                    </tr>
                    <tr>
                      <th>Website</th>
                      <td>{{ $companies->website }}</td>
                    </tr>
                  </table>
                  <!-- <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>
                    {{ $companies->name }}
                  </div>
                  <label for="name" class="col-sm-2 control-label">Email</label>
                  <span>{{ $companies->email }}</span>

                  <label for="name" class="col-sm-2 control-label">Logo</label>
                  <span>{{ $companies->logo }}</span>
                  <img src="{{ asset('images/').'/'.$companies->logo }}" id="preview-logo" width="200px" />

                  <label for="name" class="col-sm-2 control-label">Website</label>
                  <span>{{ $companies->website }}</span> -->
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{ route('companies.index') }}" type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('scripts')
<script type="text/javascript">
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          
          reader.onload = function (e) {
              $('#preview-logo').attr('src', e.target.result);
          }
          
          reader.readAsDataURL(input.files[0]);
      }
  }
  
  $("#logo").change(function(){
      readURL(this);
  });
</script>
@endsection