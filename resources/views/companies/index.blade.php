@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        Companies
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Companies</a></li>
        <li class="active">Companies Data</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Companies Data</h3>
              <div class="pull-right box-tools">
                <a href="{{ route('companies.create') }}" type="button" class="btn btn-sm btn-primary" >
                  <i class="fa fa-plus"></i> Create
                </a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th colspan="2">Logo</th>
                  <th>Website</th>
                  <th>Action</th>
                </tr>
                  <?php $no=($companies->currentpage()-1)* $companies->perpage() + 1; ?>
                @foreach($companies as $c)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $c->name }}</td>
                        <td>{{ $c->email }}</td>
                        <td>
                            {{ $c->logo }}
                        </td>
                        <td class="text-center">
                            <img src="{{ asset('storage/company/').'/'.$c->logo }}" id="preview-logo" width="50px" />
                        </td>
                        <td>{{ $c->website }}</td>
                        <td>
                            <form action="{{ route('companies.destroy', $c->id)}}" method="post">
                              <a href="{{ route('companies.show', $c->id) }}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Show</a>
                              <a href="{{ route('companies.edit', $c->id) }}" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                              @csrf
                              @method('DELETE')
                              <button class="btn btn-xs btn-danger" type="submit"><i class="fa fa-trash"></i>Delete</button>
                            </form>
                        </td>
                        <?php $no++ ?>
                    </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                {{ $companies->links() }}
              </ul>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
@endsection

@section('scripts')
<script type="text/javascript">
    
  function destroy(id) {
    swal({
      title: "Are you sure to delete this data?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $.ajax({
          type: 'DELETE',
          dataType: 'JSON',
          url: "",
          data: {
            _token: "{{ csrf_token() }}",
            id: id
          },
          success: function(res) {
            toastr.success('Successfully', 'Data has been deleted.')
            setTimeout(function(){
               window.location.reload();
            }, 1000);
          }
        })
      }

    });
  }
</script>
@endsection