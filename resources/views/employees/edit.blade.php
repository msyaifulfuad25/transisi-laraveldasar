@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        Employees
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Employees</a></li>
        <li class="active">Employees Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Employees Edit</h3>
            </div>
            <!-- /.box-header -->

            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
              </div><br />
            @endif
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="{{ route('employees.update', $employees->id) }}">
              {{ method_field('PUT') }}
              @csrf
              <div class="box-body">
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $employees->name }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="companies_id" class="col-sm-2 control-label">Companies</label>
                  <div class="col-sm-8">
                    <select name="companies_id" id="companies_id" class="form-control">
                      <option value="">-- Choose Companies --</option>
                      @foreach($items as $i )
                        <option value="{{ $i->id }}" {{$employees->companies_id == $i->id  ? 'selected' : ''}}>{{ $i->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $employees->email }}">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{ route('employees.index') }}" type="button" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
      </div>
    </section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection