@extends('layouts.app')

@section('content')
    <section class="content-header">
      <h1>
        Employees
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Employees</a></li>
        <li class="active">Employees Data</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          @if (session('success'))
              <div class="alert alert-success">
                  {{ session('success') }}
              </div>
          @endif
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Employees Data</h3>
              <div class="pull-right box-tools">
                <a href="{{ route('employees.create') }}" type="button" class="btn btn-sm btn-primary" >
                  <i class="fa fa-plus"></i> Add
                </a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Name</th>
                  <th>Companies</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
                  <?php $no=($employees->currentpage()-1)* $employees->perpage() + 1; ?>
                @foreach($employees as $e)
                    <tr>
                        <td>{{ $no }}</td>
                        <td>{{ $e->employees_name }}</td>
                        <td>
                          <a href="{{ route('companies.show', $e->companies_id) }}">{{ $e->companies_name }}</a>
                        </td>
                        <td>{{ $e->email }}</td>
                        <td>
                            <form action="{{ route('employees.destroy', $e->id)}}" method="post">
                              <a href="{{ route('employees.edit', $e->id) }}" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                              @csrf
                              @method('DELETE')
                              <button class="btn btn-xs btn-danger" type="submit"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                        </td>
                        <?php $no++ ?>
                    </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                {{ $employees->links() }}
              </ul>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
@endsection