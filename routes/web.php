<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/companies', 'CompaniesController@index')->name('companies');
// Route::post('/companies/store', 'CompaniesController@store')->name('companies-store');
Route::resource('companies', 'CompaniesController');
Route::resource('employees', 'EmployeesController');
// Route::get('/employees', 'EmployeesController@index')->name('employees');
